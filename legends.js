const 
  fs = require('fs'),
  https = require('https'),
  lol = exports,
  address = 'api.pvp.net/api/lol/',
  apiKey = fs.readFileSync( '.key', { 'encoding': 'utf8'} );

var  
  region = 'na';

lol.setRegion = function ( string ) {
  region = string;
  console.log( 'region set to', region );
};

lol.summoner = function ( summonerName, callback ) {
  ask( '/v1.4/summoner/by-name/' + summonerName, callback );
};

function ask ( query, callback ) {
  console.log( 'get', 'https://' + region + '.' + address + region + query + '?api_key=' + apiKey );
  https.get ( 'https://' + region + '.' + address + region + query + '?api_key=' + apiKey ,
    function ( response ) {
      response.setEncoding( 'utf8' );
      response.on( 'data', function ( data ) {
        callback( JSON.parse( data ) );
      } );
    }
  );
}