const
  Path = require('path'),
  Hapi = require('hapi'),
  Joi = require('joi'),
  https = require('https'),
  fs = require('fs'),
  lol = require('./legends');

var 
  server = new Hapi.Server(),
  defaultContext = { 
    name: 'badger',
    title: ''
  };

defaultContext.title = defaultContext.name;

server.connection( { port: 8406 } );

server.views( 
{
  engines: {
    html: require('handlebars')
  },
  context: defaultContext,
  relativeTo: __dirname,
  path: 'views',
  layoutPath: 'views/layouts',
  // helpersPath: 'views/helpers',
  // partialsPath: 'views/partials',
  layout: 'foundation'  
} );

server.route( {
  method: 'GET',
  path: '/',
  handler: function ( request, reply ) {
    reply.view( 'index' );
  }
} );

server.route( {
  method: 'POST',
  path: '/',
  config: {
    handler: function ( request, reply ) {
      var
        region = encodeURIComponent( request.payload.region ).toLowerCase(),
        name = encodeURIComponent( request.payload.name ).toLowerCase();
      console.log( 'POST', request.payload );
      reply.redirect('/profile/' + region + '/' + name);
    },
    validate: { 
      payload: { 
        name: Joi.string(),
        region: Joi.string()
      }
    }
  }
} );

server.route( {
  method: 'GET',
  path: '/profile/{region}/{name}',
  handler: function ( request, reply ) {
    var 
      region = encodeURIComponent( request.params.region ).toLowerCase(),
      name = encodeURIComponent( request.params.name ).toLowerCase();

    lol.setRegion( region );

    lol.summoner( name, function ( data ) {
      var profile = data[ Object.keys(data)[0] ];

      reply.view( 'profile', {
        profile: profile,
        title: profile.name + ' — ' + defaultContext.name
      } );
      // console.log( 'Retrieved info for', name );
    } );
  }
} );

server.route({
  method: 'GET',
  path: '/{filepath*}',
  handler: {
    file: function ( request ) {
      return 'files/' + request.params.filepath;
    }
  }
} );

server.start( function () {
  console.log( 'Server running at:', server.info.uri );
} );
