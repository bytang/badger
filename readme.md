#Badger

Badger is a League of Legends player statistics database. Its goal is to provide interesting recent statistics for players based on various performance metrics.

##Installation

Prerequisites:

 - [Bower](http://bower.io/ "Bower")
 - [Git](https://git-scm.com/ "Git")
 - [MongoDB](http://docs.mongodb.org/manual/installation/ "MongoDB Installation Guide")
 - [Node.js](https://nodejs.org/ "Node.js")
 - [Riot Games API key](https://developer.riotgames.com/ "Riot Games API")

Installation commands:
```
git clone https://bytang@bitbucket.org/bytang/badger.git
cd badger
npm install
bower install
```

##Usage

Badger is currently functional on the `master` and `danger` branches.

###`master`

####Configuration

Create a text file in the root directory named `.key` containing your Riot Games API key.

####Usage

Usage commands:
```
node badger
```
This will start Badger on port 8406. Navigate to http://localhost:8406/. You can search for summoners by name. The summoner profile shows their ID and level.

###`danger`

####Configuration

Create a text file in the root directory named `.key` containing your Riot Games API key.

####Usage

Usage commands:
```
node goose
```
This will start Badger on port 8406 as above. The main difference between this branch and `master` is MongoDB for caching. The summoner profile additionally shows Ranked Solo/Duo wins, losses, and win percentage. 

On first run, one MongoDB database will be created per region. These databases follow the naming scheme `badger_region` where `region` is a region code, e.g. `euw`. Inside each region database, there are two collections `Profiles` and `Ranks`. `Profiles` contains cached summoner profiles and `Ranks` contains cached SoloQ stats for individual summoner IDs. `Profile` and `Rank` are connected via matching `_id` fields.

There is also an automatic crawler that gathers current information for North American summoners. The crawler queries Badger with one profile ID incrementally. Badger will fetch the information associated with that ID and create or update the MongoDB entry for that profile. The crawler only queries every 3.5 seconds to stay within the Riot Games API 500 requests per 10 minutes developer key limit.

###`promises`

####Configuration

Create a text file in the root directory named `.api-key` containing your Riot Games API key.

####Usage

This branch is a refactor of the `danger` branch using ES6 generators and promises to decrease asynchronous code complexity. It is currently non-functional and in active development. I may include test scripts ending in `*-test.js` that are executable using `node`.